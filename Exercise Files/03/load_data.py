import tensorflow as tf
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

# Load training data set from CSV file

# We use the Panda library to read the training data. Panda makes it easy to load data into memory and work with
# the data as if it were in a virtual spreadsheet. It enables many of the same operations as a spreadsheet.
# Panda loads the data in the specified file into a data frame. dtype explicitly tells panda what type of data
# we are loading. It prevents a warning from showing up at runtime.
training_data_df = pd.read_csv("sales_data_training.csv", dtype=float)

# Pull out columns for X (data to train with) and Y (value to predict)
# axis parameter tells it to drop a column, values returns the results as an array
X_training = training_data_df.drop('total_earnings', axis=1).values
Y_training = training_data_df[['total_earnings']].values

# Load testing data set from CSV file
test_data_df = pd.read_csv('sales_data_test.csv', dtype=float)

# Pull out columns for X (data to train with) and Y (value to predict)
X_testing = test_data_df.drop('total_earnings', axis=1).values
Y_testing = test_data_df[['total_earnings']].values

# All data needs to be scaled to a small range like 0 to 1 for the neural
# network to work well. Create scalers for the inputs and outputs.

# Use the MinMaxScaler object from scikit-learn library. Pass in a feature_range parameter with the desired scale.
X_scaler = MinMaxScaler(feature_range=(0, 1))
Y_scaler = MinMaxScaler(feature_range=(0, 1))

# Scale both the training inputs and outputs.

# Call the fit_transform function on the scaler object to scale the data.
# Fit_transform means that we want it to fit our data first, figuring out how much to scale down the numbers in each
# column, and then transform or scale the data.
X_scaled_training = X_scaler.fit_transform(X_training)
Y_scaled_training = Y_scaler.fit_transform(Y_training)

# It's very important that the training and test data are scaled with the same scaler so that
# they are scaled by the same amount. This time, we just need to call transform on each scaler object.
X_scaled_testing = X_scaler.transform(X_testing)
Y_scaled_testing = Y_scaler.transform(Y_testing)

# The scaler scales the data by multiplying it by a constant number, then adding a constant number.
# It is useful to know how the data was scaled so that you can unscale the data back to the original units
# when making predictions with the neural network.
print(X_scaled_testing.shape)
print(Y_scaled_testing.shape)

print("Note: Y values were scaled by multiplying by {:.10f} and adding {:.4f}".format(Y_scaler.scale_[0], Y_scaler.min_[0]))

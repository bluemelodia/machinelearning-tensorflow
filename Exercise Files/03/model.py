import tensorflow as tf
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

# Load training data set from CSV file
training_data_df = pd.read_csv("sales_data_training.csv", dtype=float)

# Pull out columns for X (data to train with) and Y (value to predict)
X_training = training_data_df.drop('total_earnings', axis=1).values
Y_training = training_data_df[['total_earnings']].values

# Load testing data set from CSV file
test_data_df = pd.read_csv("sales_data_test.csv", dtype=float)

# Pull out columns for X (data to train with) and Y (value to predict)
X_testing = test_data_df.drop('total_earnings', axis=1).values
Y_testing = test_data_df[['total_earnings']].values

# All data needs to be scaled to a small range like 0 to 1 for the neural
# network to work well. Create scalers for the inputs and outputs.
X_scaler = MinMaxScaler(feature_range=(0, 1))
Y_scaler = MinMaxScaler(feature_range=(0, 1))

# Scale both the training inputs and outputs
X_scaled_training = X_scaler.fit_transform(X_training)
Y_scaled_training = Y_scaler.fit_transform(Y_training)

# It's very important that the training and test data are scaled with the same scaler.
X_scaled_testing = X_scaler.transform(X_testing)
Y_scaled_testing = Y_scaler.transform(Y_testing)

# Define model parameters
learning_rate = 0.001
training_epochs = 100
display_step = 5

# Define how many inputs and outputs are in our neural network (input, output nodes)
number_of_inputs = 9
number_of_outputs = 1

# Define how many neurons we want in each layer of our neural network
layer_1_nodes = 50
layer_2_nodes = 100
layer_3_nodes = 50

# Section One: Define the layers of the neural network itself

# Input Layer

# Create variable scopes in TF (to organize the code). Any variables created within this scope automatically gets a
# prefix of input to their name internally in TF.

# TF has the ability to generate diagrams of the computational graph. By putting our nodes into scopes, it helps TF
# generate more useful diagrams that are easier to understand. Everything within the same scope will be grouped
# together within the diagram.
#
# Our neural network should accept 9 floating point numbers as the input for making predictions. But each time we want
# a new prediction, the specific values we pass in will be different. We can use a placeholder node to represent that.

with tf.variable_scope('input'):
    # When we create a new node, we tell it what type of tensor to accept. The data we are passing into our network
    # will be floating point numbers, hence we pass in the tf.float32 object. We also tell it the size or shape of the
    # tensor to expect. For shape, we use None, which tells TF that our neural network can mix up batches of any size
    # and number_of_inputs tells it to expect nine values for each record in the batch (defined above).
    X = tf.placeholder(tf.float32, shape=(None, number_of_inputs))

# Layer 1
# Each fully connected layer of the neural network has three parts.
# weight - for each connection between each node and the nodes in the previous layer
# bias - for each node
# activation function - outputs the result of the layer

# We use variables instead of a placeholders, because we want TF to remember the value over time.
# Weights: For the shape, we want to have one weight for each node's connection to each node in the previous layer.
#   Our shape is an array, where one side of the array will be the number of inputs, and the other layer_1_nodes.
#   Our initializer will be the Xavier initializer, as it's one of the best initial values to use for weights.
# Biases: The size of the shape needs to be the same as the number of nodes in the layer (var defined above)
#   We can tell TF how to set the initial value of a variable by passing it one of its built-in initializer functions.
#   We want the bias value for each node to default to 0, so we pass in the tf.zeros_initializer.
# Activation function: We need to multiply the weights by the inputs and call an activation function. TF lets us do
#   this however we want. We will use matrix multiplication as a standard rectified linear unit, or relu activation
#   function. We call tf.matmul for matrix multiplication, and multiply the inputs, X, by the weights in this layer.
#   To do that, we need to add the biases, and we wrap that with a call to the relu function.
# This is how we define a standard fully-connected neural network.
with tf.variable_scope('layer_1'):
    weights = tf.get_variable(name="weights1", shape=[number_of_inputs, layer_1_nodes], initializer=tf.contrib.layers.xavier_initializer)
    biases = tf.get_variable(name="biases1", shape=[layer_1_nodes], initializer=tf.zeros_initializer)
    layer_1_output = tf.nn.relu(tf.matmul(X, weights) + biases)

# Layer 2
# Change the shape of the weights so that the input now is the output from the previous layer.
# Hence, when we calculate the output, instead of multiplying X, the initial input, we multiply by the previous layer.
with tf.variable_scope('layer_2'):
    weights = tf.get_variable(name="weights2", shape=[layer_1_nodes, layer_2_nodes], initializer=tf.contrib.layers.xavier_initializer)
    biases = tf.get_variable(name="biases2", shape=[layer_2_nodes], initializer=tf.zeros_initializer)
    layer_2_output = tf.nn.relu(tf.matmul(layer_1_output, weights) + biases)

# Layer 3
with tf.variable_scope('layer_3'):
    weights = tf.get_variable(name="weights3", shape=[layer_2_nodes, layer_3_nodes], initializer=tf.contrib.layers.xavier_initializer)
    biases = tf.get_variable(name="biases3", shape=[layer_3_nodes], initializer=tf.zeros_initializer)
    layer_3_output = tf.nn.relu(tf.matmul(layer_2_output, weights) + biases)

# Output Layer
with tf.variable_scope('output'):
    weights = tf.get_variable(name="weights4", shape=[layer_3_nodes, number_of_outputs], initializer=tf.contrib.layers.xavier_initializer)
    biases = tf.get_variable(name="biases4", shape=[number_of_outputs], initializer=tf.zeros_initializer)
    prediction = tf.nn.relu(tf.matmul(layer_3_output, weights) + biases)


# Section Two: Define the cost function of the neural network that will measure prediction accuracy during training
# Cost function is the same as loss function. It tells us how wrong the neural network is when trying to predict the
# correct output for a single piece of training data.

# Y is the node for the expected value, we will feed it in during training. Like the input values, this is a placeholder
# node because we will feed in a new value each time. For shape we pass None, 1 since there is just a single output.
# For the cost, we calculate the mean square error between what the neural network predicted and what we expected it
# to calculate. To do that, we call the tf.squared_difference function and pass in the actual prediction and expected
# value (hence, our prediction from the neural network which we defined above). This gives us the squared difference.
# To turn it into the mean square difference, we get the average value of that difference.
with tf.variable_scope('cost'):
    Y = tf.placeholder(tf.float32, shape=(None, 1))
    cost = tf.reduce_mean(tf.squared_difference(prediction, Y))


# Section Three: Define the optimizer function that will be run to optimize the neural network

# We call one of the optimizers supplied by TF. We pas the learning rate and tell the Adam Optimizer which variable
# we want it to minimize. So we pass in our cost function as the value to minimize. This tells TF that whenever we
# tell it to execute the optimizer, it should run one iteration of the Adam optimizer in an attempt to make the cost
# value smaller.

with tf.variable_scope('train'):
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
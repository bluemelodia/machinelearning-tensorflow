import os
import tensorflow as tf

# TensorFlow always works with tensors, multi-dimensional arrays. They expect and return arrays.

# Turn off TensorFlow warning messages in program output
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Define the computational graph

# X & Y are placeholder nodes that get assigned new values each time we make a calculation.
# Parameters: data-type and name, which will show up when we look at graphical visualizations of our model.
X = tf.placeholder(tf.float32, name="X")
Y = tf.placeholder(tf.float32, name="Y")

# Define the node that does the addition operation. Pass in X and Y nodes to the addition node,
# telling TensorFlow to link these nodes on the computation graph. We as TF to pull the values from X and Y
# and add the result.
addition = tf.add(X, Y, name="addition")


# Create the session to execute operations in the graph
with tf.Session() as session:
    # Call session.run to ask the session to run operations on the computational graph.
    # Pass in the operation we want to run (addition). The additional operation, when run, sees that it needs to
    # grab the values of the X and Y nodes, so we feed in values for X and Y. We do that by supplying a parameter
    # called feed_dict, passing in array values for X and Y.
    result = session.run(addition, feed_dict={X: [1, 2, 10], Y: [4, 2, 10]})

    print(result)

